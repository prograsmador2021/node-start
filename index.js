// const http = require('http')
const { response } = require('express')
const express = require('express')
const app = express()
const notes = [
    {
        "id":1,
        "content":"Un titulo",
    },
    {
        "id":2,
        "content":"Un titulo",
    },
    {
        "id":3,
        "content":"Otro titulo",
    },
    {
        "id":4,
        "content":"Otro titulo mas",
    }
]
// const app = http.createServer( (request, response) => {
//     response.writeHead(200, {'content-type':'application-json'})
//     response.end(JSON.stringify(notes))
// } )

app.get('/', (resquest, response) => {
    response.send('<h1>Pagina principal</h1>')
} )

app.get('/notas/', (request,  response) => {
    response.json(notes)
} )

const port = 3001
app.listen(port)
console.log(`Server is runngin on port ${port}`)